from django.shortcuts import render
from django.http import HttpResponse

from . import models

# Create your views here.
def index(request):
    context = {
        "name": "Thomas",
        "liste": ["Eins", "Zwei", "Drei", "Vier", "Fünf"]
    }
    return render(request, 'hello/index.html', context)

def einkaufen(request):
    context = {

    }
    produkte = models.Produkt.objects.all()
    return render(request, "hello/einkaufsliste")